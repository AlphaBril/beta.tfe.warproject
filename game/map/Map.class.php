<?PHP

require_once("game/ships/Army.class.php");

class Map
{
	public static $verbose = false;
	private $_army;
	private $_mapx;
	private $_mapy;

	public function doc()
	{
	}
	public function asteroid($x, $y)
	{
		if ($x > 25 && $x < ($this->_mapx - 25) && $y > 0 && $y < $this->_mapy)
		{
			if (rand(0, 250) == 1)
				return 1;
			else
				return 0;
		}
		else
			return 0;
	}
	public function printmap()
	{
		$ret = "<table>";
		$y = 0;
		while ($y < $this->_mapy)
		{
			$ret = $ret . "<tr>";
			$x = 0;
			while ($x < $this->_mapx)
			{
				if ($this->_army->putship($x, $y) == 1)
					$ret = $ret . "<td id='$x $y' style='background-color: rgba(55, 55, 55, 0.5)'>" . $this->_army->ship($x, $y) . "</td>";
				else if ($this->asteroid($x, $y) == 1)
					$ret = $ret . "<td id='$x $y' style='background-color: red;'></td>";
				else
					$ret = $ret . "<td id='$x $y' style='background-color: rgba(55, 55, 55, 0.5)' onclick='interact($x, $y, this)'></td>";
				$x++;
			}
			$ret = $ret . "</tr>";
			$y++;
		}
		$ret = $ret . "</table>";
		print($ret);
	}
	public function __construct(array $kwargs)
	{
		$this->_mapx = $kwargs['x'];
		$this->_mapy = $kwargs['y'];
		$this->_army = new Army('Necron', 'Chaos', $kwargs['x'], $kwargs['y']);
		if (self::$verbose == true)
			print('New instance of map x=' . $this->_mapx . ', y=' . $this->_mapy . ', constructed');
	}
	public function __destruct()
	{
		if (self::$verbose == true)
			print('Instance of map x=' . $this->_mapx . ', y=' . $this->_mapy . ', destructed');
	}
}

?>
