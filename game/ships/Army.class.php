<?PHP

class Army
{
	public static $verbose = false;
	private $_mapx;
	private $_mapy;
	private $_arm1;
	private $_arm2;
	private $_data1;
	private $_data2;

	public function putship($x, $y)
	{
		if ($x == 0 || $x == ($this->_mapx - 1))
		{
			if ($y == ($this->_mapy / 2))
				return 1;
		}
	}
	public function ship($x, $y)
	{
		if ($y == ($this->_mapy / 2) && $x == 0)
		{
			return ("<img id='CairnTomb' src='image/necron/Cairn%20class%20tomb%20ship.png' style='height: 134px;width: 150px;background-color: rgba(0, 0, 0, 0);position: absolute;' onclick='interactship($x, $y, this)'>");
		}
		else if ($y == ($this->_mapy / 2) && $x == ($this->_mapx - 1))
		{
			return ("<img id='PlanetKiller' src='image/chaos/Planet%20killer%20class%20battleship.png' style='height: 134px;width: 250px;background-color: rgba(0, 0, 0, 0);transform: rotate(180deg);margin-left: -240px;position: absolute;' onclick='interactship($x, $y, this)'>");
		}
	}
	public function __construct($arm1, $arm2, $mapx, $mapy)
	{
		if ($arm1 == "Necron" && $arm2 == "Chaos")
		{
			$this->_arm1 = $arm1;
			$this->_arm2 = $arm2;
			$this->_data1 = file_get_contents("game/ships/Necron/ships.txt");
			$this->_data2 = file_get_contents("game/ships/Chaos/ships.txt");
		}
		$this->_mapx = $mapx;
		$this->_mapy = $mapy;
		if (self::$verbose == true)
			print($this->_arm1 . " rise for player1 and " . $this->_arm2 . " rise for player2.");
	}
	public function __destruct()
	{
		if (self::$verbose == true)
			print($this->_arm1 . " army and " . $this->_arm2 . " army fall appart...");
	}
}

?>
