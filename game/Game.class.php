<?PHP

require_once("map/Map.class.php");

class Game
{
	private $_player1 = "";
	private $_player2 = "";
	private $_map = array();
	public static $verbose = false;

	public function doc()
	{
		$ret = file_get_contents("Game.doc.txt");
		print($ret);
	}
	public function __construct( array $kwargs)
	{
		$this->_player1 = $kwargs['p1'];
		$this->_player2 = $kwargs['p2'];
		$this->_map = new Map ( array ( 'x' => $kwargs['mapx'], 'y' => $kwargs['mapy'] ) );
		$this->_map->printmap();
		if (self::$verbose == true)
			print('New instance with player1 as ' . $this->_player1 . ', player2 as ' . $this->_player2
			. ' and a ' . $kwargs['mapx'] . 'x' . $kwargs['mapy'] . 'map constructed');
	}
	public function __destruct()
	{
		if (self::$verbose == true)
			print('Instance with player1 as ' . $this->_player1 . ', player2 as ' . $this->_player2
			. ' and a ' . $kwargs['mapx'] . 'x' . $kwargs['mapy'] . 'map destructed');
	}
}

?>
