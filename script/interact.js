selected = 0;
function interact(x, y, cell)
{
	var color = cell.style.backgroundColor;
	if (shipselected[0] == -1 && shipselected[1] == -1)
	{
		if (color == "rgba(55, 55, 55, 0.5)" && selected == 0)
		{
			console.log("cell selected :", x, y);
			color = "blue";
			selected = 1;
		}
		else if (color == "blue")
		{
			console.log("cell unselected :", x, y);
			color = "rgba(55, 55, 55, 0.5)";
			selected = 0;
		}
		cell.style.backgroundColor = color;
	}
	else
	{
		cell.appendChild(shipselected[2]);
		console.log("ship moved");
	}
}
